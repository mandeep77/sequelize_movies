const path = require('path');
const express = require("express");
const fs = require("fs");
const router = express.Router();
const db = require(path.join(__dirname, '../', 'models'));
const { directors, actors, genres, movies: Movies } = require(path.join(__dirname, '../', 'models', 'index.js'));

router.get('/directors', (req, res) => {
    fs.readFile(path.join(__dirname, '../', 'movies.json'), 'utf-8', (err, data) => {
        if (err) {
            res.status(400).json({
                msg: 'bad request'
            })
                .end();;
        }
        else {
            let director_array = [];
            JSON.parse(data).forEach(movie => {

                if (!(director_array[movie.Director])) {

                    let obj = {
                        director_name: movie.Director
                    }

                    director_array.push(movie.Director);
                    directors.create(obj)
                }

            });

            res.status(200).json({
                msg: "data uploaded in directors table"
            }
            );
            res.end();

        }
    })
})

router.get('/actors', (req, res) => {
    fs.readFile(path.join(__dirname, '../', 'movies.json'), 'utf-8', (err, data) => {
        if (err) {
            res.status(400).json({
                msg: 'bad request'
            })
                .end();;
        }
        else {

            let movie_array = [];
            JSON.parse(data).forEach(movie => {
                if (!(movie_array[movie.Actor])) {

                    let actor = {
                        actor_name: movie.Actor

                    }

                    movie_array.push(movie.Actor);
                    actors.create(actor)
                }
            });
            res.status(200).json({
                msg: "data uploaded in actors table"
            });
            res.end();

        }
    })



})

router.get('/genre', (req, res) => {
    fs.readFile(path.join(__dirname, '../', 'movies.json'), 'utf-8', (err, data) => {
        if (err) {
            res.status(400).json({
                msg: 'bad request'
            })
                .end();
        }

        else {

            let movie_array = [];
            JSON.parse(data).forEach(movie => {

                if (!(movie_array[movie.Genre])) {

                    let genre = {
                        genre_name: movie.Genre

                    }

                    genres.create(genre)
                    movie_array.push(movie.Genre);
                }
            });
            res.status(200).json({
                msg: "data uploaded in genres table"
            });
            res.end();

        }
    })



})

router.get('/movie', (req, res) => {
    fs.readFile(path.join(__dirname, '../', 'movies.json'), 'utf-8', async (err, data) => {
        if (err) {
            res.status(400).json({
                msg: 'bad request'
            });
            res.end();
        }
        else {

            let movie_data = [];
            let actorData = await actors.findAll();
            //console.log(actorData)
            let directorData = await directors.findAll();
            let genreData = await genres.findAll();

            JSON.parse(data).forEach(movie => {
                let foreign_table_data = {};

                actorData.forEach(actor => {

                    if (movie.Actor == actor.actor_name) {
                        foreign_table_data["actorId"] = actor.id;

                    }
                })

                directorData.forEach(director => {

                    if (movie.Director == director.director_name) {
                        foreign_table_data["directorId"] = director.id;

                    }
                })

                genreData.forEach(genre => {

                    if (movie.Genre == genre.genre_name) {
                        foreign_table_data["genreId"] = genre.id;

                    }
                })

                let template = {};
                template.Rank = movie.Rank;
                template.Title = movie.Title;
                template.Description = movie.Description;
                template.Runtime = movie.Runtime;
                //template.Genre = movie.Genre;
                template.Rating = movie.Rating;
                template.Metascore = movie.Metascore;
                template.Votes = movie.Votes;
                template.Gross_Earning_in_Mil = movie.Gross_Earning_in_Mil;
                template.Year = movie.Year;
                foreign_table_data = { ...template, ...foreign_table_data };

                movie_data.push(foreign_table_data);;

            })

            movie_data.forEach(data => {
                Movies.create(data).catch(console.log);

            })
            res.status(200).json({
                msg: "added data into movie table"
            });
            res.end();
        }

    })
})



module.exports = router;