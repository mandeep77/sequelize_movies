const path = require('path');
const express = require("express");
const router = express.Router();
const db = require(path.join(__dirname, '../', 'models'));
const { directors, actors, genres, movies: Movies, users } = require(path.join(__dirname, '../', 'models', 'index.js'));
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const bcrypt = require('bcrypt')
dotenv.config();

function access(req, res, next) {
    const bearerHeader = req.headers['authorization'];

    if (!(bearerHeader === undefined)) {

        const bearer = bearerHeader.split(' ');
        const accessToken = bearer[1];
        req.token = accessToken;
        next();

    }
    else {

        res.status(400).json({
            msg: "invalid authorization please enter the token"
        })
        res.end();
    }
}

// router.get('/login', (req, res) => {
//     //auth user
//     const user = { name: 'Nik Klaus Mikaelson' }
//     const token = jwt.sign({ user }, process.env.my_access_token)
//     res.json({
//         token: token
//     })

// })

// const users = [{
//     name: 'Nik Klaus Mikaelson',
//     password: 'klaus@123'
// }]

router.post('/users', async (req, res) => {
    console.log(req.body.name);
    try {

        const hashedPassword = await bcrypt.hash(req.body.password, 10)
        const user = { name: req.body.name, password: hashedPassword }
        //console.log(user)

        users.create(user);

        res.status(201).send('user added')
        res.end();
    } catch(err) {

        res.status(500).send(err)

    }
})

router.get('/uu', async (req, res) => {
    const movies = await users.findAll();
    res.send(movies);
})
// router.get('/u', (req, res) => res.json(users))

router.post('/users/login', async (req, res) => {

    const user = await users.findOne(
        {
            where: {
                name: req.body.name
            }
        });

    //const user = users.find(user => user.name === req.body.name)
    if (user == null) {
        return res.status(400).send('Cannot find user').end();
    }
    try {
        if (await bcrypt.compare(req.body.password, user.password)) {

            const token = jwt.sign({ user }, process.env.my_access_token)
            res.json(
                {
                    msg: 'success',
                    token: token
                })
            res.end();

        } else {
            res.send('Not Allowed')
        }
    } catch (err) {
        res.status(500).send(err).end();
    }
})



router.get("/all", access, (req, res) => {

    jwt.verify(req.token, process.env.my_access_token, async (err, data) => {

        if (err) {
            res.status(401).json({ msg: "invalid token" })
            res.end()
        }
        else {

            const movies = await Movies.findAll({ include: ["director", "actor", "genre"] });
            res.status(200).send(movies);
            res.end();
        }
    })

});

router.get("/find/:id", access, (req, res) => {
    jwt.verify(req.token, process.env.my_access_token, async (err, data) => {

        if (err) {
            res.status(401).json({ msg: "invalid token" })
            res.end()
        }
        else {
            try {
                // let movie = await Movies.findAll({
                //     where: {
                //         id: req.params.id,
                //         //  include: ["director", "actor", "genre"]
                //     },
                //     include: ["director", "actor", "genre"]
                // })
                // if(movie===[]){
                //     res.send('no').end();
                // }
                // else
                // {
                //     res.send(movie)
                // }
                return Movies
                    .findByPk(req.params.id, {
                        include: ["director", "actor", "genre"],
                    })
                    .then((movie) => {
                        if (!movie) {
                            return res.status(404).send({
                                message: 'movie Not Found',
                            });
                        }
                        return res.status(200).send(movie);

                    })
                    .catch((error) => {
                        console.log(error);
                        res.status(400).send(error);
                        res.end();
                    });

            }
            catch (err) {
                res.status(400).json({ msg: "sorry no data found" }).end();
            }
        }
        // res.status(400).json({ msg: "bad request" });

    });
})


router.post("/new", access, (req, res) => {
    jwt.verify(req.token, process.env.my_access_token, async (err, data) => {
        if (err) {
            res.status(401).json({ msg: "invalid token" })
            res.end()
        }
        else {

            try {
                let newMem = {
                    Rank: req.body.Rank,
                    Title: req.body.Title,
                    Description: req.body.Description,
                    Runtime: req.body.Runtime,
                    Genre: req.body.Genre,
                    Rating: req.body.Rating,
                    Metascore: req.body.Metascore,
                    Votes: req.body.Votes,
                    Gross_Earning_in_Mil: req.body.Gross_Earning_in_Mil,
                    Director: req.body.Director,
                    Actor: req.body.Actor,
                    Year: req.body.Year
                };
                read();
                async function read() {
                    let actorData = await actors.findAll();
                    let directorData = await directors.findAll();
                    let genreData = await genres.findAll();

                    actorData.forEach(actor => {
                        if (actor.actor_name == newMem.Actor) {
                            newMem["actorId"] = actor.id;
                        }
                    })

                    directorData.forEach(director => {
                        if (director.director_name == newMem.Director) {
                            newMem["directorId"] = director.id;
                        }
                    })

                    genreData.forEach(genre => {
                        if (genre.genre_name == newMem.Genre) {
                            newMem.genreId = genre.id;
                        }
                    })
                    //res.send(newMem);
                    Movies.create(newMem);
                    res.status(200).json({ msg: "updated" })
                    res.end();
                }
            } catch (err) {

                res.status(400).json({ msg: "bad request" })
                res.end();
            }
        }
    })
})

router.put("/update/:id", access, (req, res) => {
    jwt.verify(req.token, process.env.my_access_token, async (err, data) => {
        if (err) {
            res.status(401).json({ msg: "invalid token" })
            res.end()
        }
        else {


            try {
                Movies.findAll({
                    where: {
                        id: req.params.id,
                        //  include: ["director", "actor", "genre"]
                    },
                    include: ["director", "actor", "genre"]
                }).then(() => {
                    let newMem = {
                        Rank: req.body.Rank,
                        Title: req.body.Title,
                        Description: req.body.Description,
                        Runtime: req.body.Runtime,
                        Genre: req.body.Genre,
                        Rating: req.body.Rating,
                        Metascore: req.body.Metascore,
                        Votes: req.body.Votes,
                        Gross_Earning_in_Mil: req.body.Gross_Earning_in_Mil,
                        Director: req.body.Director,
                        Actor: req.body.Actor,
                        Year: req.body.Year
                    };
                    read();
                    async function read() {
                        let actorData = await actors.findAll();
                        let directorData = await directors.findAll();
                        let genreData = await genres.findAll();

                        actorData.forEach(actor => {
                            if (actor.actor_name == newMem.Actor) {
                                newMem["actorId"] = actor.id;
                            }
                        })

                        directorData.forEach(director => {
                            if (director.director_name == newMem.Director) {
                                newMem["directorId"] = director.id;
                            }
                        })

                        genreData.forEach(genre => {
                            if (genre.genre_name == newMem.Genre) {
                                newMem.genreId = genre.id;
                            }
                        })
                        //res.send(newMem);
                        Movies.update(newMem, {
                            where: {
                                id: req.params.id
                            }
                        });
                        res.json({ msg: "updated member" })
                        res.end();
                    }

                });

            } catch (err) {
                res.status({ msg: "bad request" })
                res.end();
            }
        }
    })
})

router.delete("/delete/:id", access, (req, res) => {
    jwt.verify(req.token, process.env.my_access_token, (err, data) => {
        if (err) {
            res.status(401).json({ msg: "invalid token" })
            res.end()
        }
        else {

            try {
                Movies.destroy({
                    where: {
                        id: req.params.id
                    }
                }).then(() => res.send({ msg: "movie deleted" }));
            } catch (err) {
                res.status(400).json({ msg: "bad" })
                res.end();
            }
        }
    })
});

module.exports = router;