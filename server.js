const express = require("express");
const { directors, actors, genres, movies: Movies } = require("./models/index");
const path = require("path");

const app = express();
app.use(express.urlencoded({ extended: false }));
app.use(express.json());


const db = require("./models");
// db.sequelize.sync();

// db.sequelize.sync({ force: true }).then(() => {
//     console.log("Drop and re-sync db.");
// });

app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname,'index.html'));
});

app.get("/register", (req, res) => {
    res.sendFile(path.join(__dirname,'register.html'));
});
app.get("/login", (req, res) => {
    res.sendFile(path.join(__dirname,'login.html'));
});

const insertTable = require("./routes/insertTable.js");
app.use("/insert", insertTable);

const apiRoutes = require("./routes/apiRoute.js");
app.use("/api", apiRoutes);




const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
