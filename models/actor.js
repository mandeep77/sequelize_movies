module.exports = (sequelize, Sequelize) => {
  const Actor = sequelize.define('actor', {
    actor_name: {
      type: Sequelize.STRING
    }
  });

  return Actor;
};