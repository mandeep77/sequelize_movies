const connection = require("../config/connection.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(connection.db, connection.user, connection.password, {
  host: connection.HOST,
  dialect: connection.dialect,

  pool: {
    max: connection.pool.max,
    min: connection.pool.min,
    acquire: connection.pool.acquire,
    idle: connection.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.actors = require("./actor.js")(sequelize, Sequelize);
db.directors = require("./director.js")(sequelize, Sequelize);
db.genres = require("./genre")(sequelize, Sequelize);
db.movies = require("./movie")(sequelize, Sequelize);
db.users = require("./users")(sequelize, Sequelize)


db.directors.hasMany(db.movies, { as: "movies"});
db.movies.belongsTo(db.directors, { 
    foreignKey: "directorId",
    as: "director"
})


db.actors.hasMany(db.movies, { as: "movies"});
db.movies.belongsTo(db.actors, { 
    foreignKey: "actorId",
    as: "actor"
})


db.genres.hasMany(db.movies, { as: "movies"});
db.movies.belongsTo(db.genres, { 
    foreignKey: "genreId",
    as: "genre"
})


module.exports = db;
