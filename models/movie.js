module.exports = (sequelize, Sequelize) => {
    const Movie = sequelize.define('movie', {


        Rank: {
            type: Sequelize.INTEGER
        },
        Title: {
            type: Sequelize.STRING
        },
        Description: {
            type: Sequelize.STRING
        },
        Runtime: {
            type: Sequelize.INTEGER
        },
        Rating: {
            type: Sequelize.INTEGER
        },
        Metascore: {
            type: Sequelize.STRING
        },
        Votes: {
            type: Sequelize.INTEGER
        },
        Gross_Earning_in_Mil: {
            type: Sequelize.STRING
        },
        Year: {
            type: Sequelize.DATEONLY
        }


    });

    return Movie;
};