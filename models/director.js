module.exports = (sequelize, Sequelize) => {
  const Director = sequelize.define('director', {
    director_name: {
      type: Sequelize.STRING
    }
  });

  return Director;
};