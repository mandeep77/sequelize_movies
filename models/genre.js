module.exports = (sequelize, Sequelize) => {
  const Genre = sequelize.define('genre', {
    genre_name: {
      type: Sequelize.STRING,
    }
  });

  return Genre;
};