const config = require('./config')


let connection = {
    host: config.sequelize.HOST,
    user: config.sequelize.USER,
    password: config.sequelize.PASSWORD,
    database: config.sequelize.DB,
    dialect: config.sequelize.dialect,
    pool: config.sequelize.pool
};




module.exports = connection; 